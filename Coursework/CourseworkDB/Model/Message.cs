﻿using System.ComponentModel.DataAnnotations;

namespace CourseworkDB.Model
{
    public class Message
    {
        [Key]
        public int Id{ get; set; }
        [Required]
        public string Text{ get; set; }
        public virtual User User { get; set; }
        public virtual Reservation Reservation { get; set; }
    }
}

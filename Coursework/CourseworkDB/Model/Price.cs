﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CourseworkDB.Model
{
    public class Price
    {
        [Key]
        public int Id { get; set; }
        public DateTime LastUppdate { get; set; }
        public decimal NewPrice { get; set; }
        public virtual Room Room { get; set; }
    }
}

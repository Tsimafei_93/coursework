﻿namespace CourseworkDB.Model
{
    public class Picture
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public byte[] File { get; set; }
        public virtual Room Room { get; set; }

    }
}
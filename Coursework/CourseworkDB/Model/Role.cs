﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CourseworkDB.Model
{
    public class Role
    {
        [Key]
        [Required]
        public  int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }

    }
    //public enum RoleEnum
    //{
    //    Client,
    //    User,
    //    Admin
    //}
}

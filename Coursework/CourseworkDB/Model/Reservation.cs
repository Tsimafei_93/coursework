﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CourseworkDB.Model
{
    public class Reservation
    {
        [Key]
        
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public virtual User Client { get; set; }
       // public virtual Payment Payment { get; set; }
       // public virtual ICollection<Message> Messages { get; set; }
        public virtual Room Room { get; set; }

    }
}

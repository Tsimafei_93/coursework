﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CourseworkDB.Model
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(20)]
        public string Firstname { get; set; }
       
        [Required]
        [MaxLength(20)]
        public string Lastname { get; set; }

        [Required]
        [MaxLength(20)]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
 
        [MaxLength(50)]
        public string Adress { get; set; }

        [Phone]
        public string Phone { get; set; }
        public bool IsDeleted { get; set; }
        
        public virtual Role Role { get; set; }
      //  public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Room> Rooms { get; set; }
      // public virtual ICollection<Payment> Payments { get; set; }
    }
}

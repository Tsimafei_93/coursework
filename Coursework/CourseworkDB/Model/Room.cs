﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CourseworkDB.Model
{
    public class Room
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(200)]
        public string Name { get; set; }

        [Required]
        [MaxLength(200)]
        public string Serial { get; set; }

        [Required]
        [MaxLength(200)]
        public string Adress { get; set; }
       public decimal ActualPrice { get; set; }
        public string Desc { get; set;}

       // [NotMapped]
        //public Price ActualPrice { get => Prices.AsQueryable().OrderByDescending(p => p.LastUppdate).FirstOrDefault(); }

        public virtual User Owner { get; set; }
      //  public virtual ICollection<Price> Prices { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
      //  public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
    }
}

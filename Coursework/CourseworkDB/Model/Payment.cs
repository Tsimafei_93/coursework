﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CourseworkDB.Model
{
    public class Payment
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public decimal Amount { get; set; }

        public int CardNumber { get; set; }
        public virtual User Users { get; set; }
        public virtual ICollection<Reservation> Reservations { get; set; }
    }
}

﻿using CourseworkDB.Model;
using CourseworkDB.Repository;
using System.Threading;

namespace CourseworkDB
{
    public class UnitOfWork : IUnitOfWork
    {
        public readonly ApplicationContext _context;

        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
        }

        public UserRepository<User> _users;
        public UserRepository<User> Users
        {
            get => _users is null ? _users = new UserRepository<User>(_context) : _users;
        }

        public RoomRepository<Room> _rooms;
        public RoomRepository<Room> Rooms
        {
            get => _rooms is null ? _rooms = new RoomRepository<Room>(_context) : _rooms;
        }

        public GenericRepository<Role> _role;
        public GenericRepository<Role> Roles
        {
            get => _role is null ? _role = new GenericRepository<Role>(_context) : _role;
        }

        public GenericRepository<Reservation> _reservation;
        public GenericRepository<Reservation> Reservations
        {
            get => _reservation is null ? _reservation = new GenericRepository<Reservation>(_context) : _reservation;
        }

        public GenericRepository<Picture> _picture;
        public GenericRepository<Picture> Pictures
        {
            get => _picture is null ? _picture = new GenericRepository<Picture>(_context) : _picture;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}

﻿using CourseworkDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseworkDB.Repository
{
    public class RoomRepository<T>: GenericRepository<Room>
    {
        public RoomRepository(ApplicationContext context):
            base (context) {}

        public Room find(int id)
        {
            return _context.Find<Room>(id);
        }

        public IQueryable<Room> readAll()
        {
            return _context.Set<Room>();
        }

        public virtual Room update(Room entity)
        {
            return _context.Update(entity).Entity;
        }

        public virtual void delete(Room entity)
        {
            _context.Remove(entity);
        }

        public Room add(Room entity)
        {
            return _context.Add(entity).Entity;
        }

    }
}

﻿using CourseworkDB.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
namespace CourseworkDB.Repository
{
    public class GenericRepository<T>
        : IGenericRepository<T>
        where T : class
    {
        protected readonly ApplicationContext _context;
        public GenericRepository(ApplicationContext context)
        {
            _context = context;
        }

        public T Read(int id)
        {
            return _context.Find<T>(id);
        }

        public virtual IQueryable<T> ReadAll()
        {
            return _context.Set<T>();
        }

        public virtual T Update(T entity)
        {
            return _context.Update<T>(entity).Entity;
        }

        public virtual void Delete(T entity)
        {
            _context.Remove<T>(entity);
        }

        public T Create(T entity)
        {
            return _context.Add(entity).Entity;
        }
    }
}

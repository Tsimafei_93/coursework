﻿using CourseworkDB.Model;


namespace CourseworkDB.Repository
{
    public class UserRepository<T> : GenericRepository<User>
    {
        public UserRepository(ApplicationContext context) :
            base(context)
        { }

        public override void Delete(User entity)
        {
            entity.IsDeleted = true;
            _context.Update(entity);
        }
    }
}

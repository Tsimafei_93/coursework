﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseworkDB.Repository.Interfaces
{
    public interface IGenericDelete<T>
        where T : class
    {
        public void Delete(T entity);
   
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseworkDB.Repository.Interfaces
{
    public interface IGenericRepository<T> : IGenericDelete<T>, IGenericRead<T>, IGenericCreate<T>
    where T : class
    {
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseworkDB.Repository.Interfaces
{
    public interface IGenericCreate<T>
        where T:class
    {
        public T Create(T entity);

        public T Update(T entity);
    }
}

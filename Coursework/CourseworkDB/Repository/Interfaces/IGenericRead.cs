﻿using System.Linq;

namespace CourseworkDB.Repository.Interfaces
{
    public interface IGenericRead<T>
        where T : class
    {
        public T Read(int id);
        public IQueryable<T> ReadAll();
    }
}

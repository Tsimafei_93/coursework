﻿using CourseworkDB.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace CourseworkDB
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        //public DbSet<Price> Prise { get; set; }
        public DbSet<Room> Rooms { get; set; }
       // public DbSet<Message> Messages { get; set; }
       // public DbSet<Payment> Payments { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Picture> Pictures { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            //string connectionString = config.GetConnectionString("DefaultConnection");
            //optionsBuilder.UseSqlServer(connectionString);
            optionsBuilder.UseSqlServer("Data Source=DESKTOP-CJPQ1T0\\SQLEXPRESS;Initial Catalog=СourseworkDB;Integrated Security=True;");
        }

    }

}

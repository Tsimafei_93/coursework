﻿using CourseworkDB.Model;
using CourseworkDB.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseworkDB
{
    public interface IUnitOfWork
    {
        UserRepository<User> Users { get; }

        RoomRepository<Room> Rooms { get; }

        GenericRepository<Role> Roles { get; }

        GenericRepository<Reservation> Reservations { get; }

        GenericRepository<Picture> Pictures { get; }

        void SaveChanges();
    }
}

﻿using Microsoft.AspNetCore.Http;

namespace WebApp.Models.Room
{
    public class BufferImage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IFormFile picFile { get; set; }
        public byte[] Image { get; set; }
        public int RoomId { get; set; }
    }
}

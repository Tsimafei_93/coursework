﻿using CourseworkDB.Model;
using System.Collections.Generic;

namespace WebApp.Models
{
    public class RoomInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Serial { get; set; }
        public string Adress { get; set; }
        public string Owner { get; set; }
        public decimal ActualPrice { get; set; }
        public string Desc { get; set; }
        public List<Picture> Pictures { get; set; }
       // public ICollection<Reservation> Reservations { get; set; }
       // public ICollection<Message> Messages { get; set; }
    }
}

﻿using CourseworkDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class ReservationInfo
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Room { get; set; }
        public string Adress { get; set; }

    }
}

﻿using CourseworkDB.Model;
using System.Collections.Generic;

namespace WebApp.Models
{
    public class UserInfo
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public bool IsDelete { get; set; }
        public Role Role { get; set; }
        public ICollection<Message> Messages { get; set; }
        public ICollection<RoomInfo> Rooms { get; set; }
        public ICollection<Payment> Payments { get; set; }

    }
}

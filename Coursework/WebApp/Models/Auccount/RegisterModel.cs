﻿using CourseworkDB.Model;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Не указан Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль введен неверно")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Имя введено неверно")]
        [MaxLength(50)]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Фамилия введена неверно")]
        [MaxLength(50)]
        public string Lastname { get; set; }

        public int Role{ get; set; }
    }
}

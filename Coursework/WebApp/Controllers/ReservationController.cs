﻿using AutoMapper;
using CourseworkDB.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ReservationController : Controller
    {
        private readonly IReservationService _reservationService;
        private readonly IUserService _userService;
        private readonly IRoomService _roomService;
        public ReservationController(IReservationService reservationService, IUserService userService, IRoomService roomService)
        {
            _reservationService = reservationService;
            _userService = userService;
            _roomService = roomService;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult GetAll()
        {

            var res = _reservationService.GetAll();
            var model = new List<ReservationInfo>();

            model = res
                .Select(u => new ReservationInfo
                {
                    Id = u.Id,
                    Adress = u.Room.Adress,
                    Room = u.Room.Name,
                    Firstname = u.Client.Firstname,
                    Lastname = u.Client.Lastname,
                    StartTime = u.StartTime,
                    EndTime = u.EndTime,
                })
            .ToList();

            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetUserAll()
        {
            string userId = User.FindFirst(x => x.Type == ClaimTypes.UserData).Value;
            var id = Convert.ToInt32(userId);


            var res = _reservationService.GetAll().Where(i => i.Client.Id == id).ToList();
            var model = new List<ReservationInfo>();

            model = res
                .Select(u => new ReservationInfo
                {
                    Id = u.Id,
                    Adress = u.Room.Adress,
                    Room = u.Room.Name,
                    Firstname = u.Client.Firstname,
                    Lastname = u.Client.Lastname,
                    StartTime = u.StartTime,
                    EndTime = u.EndTime,
                })
            .ToList();

            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetInfo(int id)
        {
            var res = _reservationService.Read(id);
            var model = new ReservationInfo()
            {
                Id = res.Id,
                Adress = res.Room.Adress,
                Room = res.Room.Name,
                Firstname = res.Client.Firstname,
                Lastname = res.Client.Lastname,
                StartTime = res.StartTime,
                EndTime = res.EndTime,
            };
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult Create(ReservationInfo newReserv, int roomId, DateTime start, DateTime end )
        {
            string userId = User.FindFirst(x => x.Type == ClaimTypes.UserData).Value;
            var id = Convert.ToInt32(userId);

            var res = new Reservation()
            {
                Id = newReserv.Id,
                StartTime = start,
                EndTime = end,
                Client = _userService
                    .Get(id),
                Room = _roomService.Get(roomId)
            };

            _reservationService.Add(res);
            var result = true;

           return Json(result);
           //return Redirect(Url.Action("GetUserAll", "Reservation"));
        }
    }
}

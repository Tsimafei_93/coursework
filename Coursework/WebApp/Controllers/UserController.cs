﻿using CourseworkDB.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService, IRoomService roomService)
        {
            _userService = userService;
        }


        [Authorize(Roles = "Admin")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            var model = new List<UserInfo>();

            model = users
                .Select(u => new UserInfo
                {
                    Id = u.Id,
                    Firstname = u.Firstname,
                    Lastname = u.Lastname,
                    Email = u.Email,
                    Role = new Role
                    {
                        Name = u.Role.Name,

                    },
                    IsDelete = u.IsDelete,
                }).ToList();
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetAccount(int id)
        {
            string userId = User.FindFirst(x => x.Type == ClaimTypes.UserData).Value;
            id = Convert.ToInt32(userId);

            var user = _userService.Get(id);
            var model = new UserInfo
            {
                Id = user.Id,
                Firstname = user.Firstname,
                Lastname = user.Lastname,
                Email = user.Email,
                Adress = user.Adress,
                Phone = user.Phone,
                Password = user.Password,
                Role = user.Role,
            };
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetInfo(int id)
        {
            var user = _userService.Get(id);
            var model = new UserInfo
            {
                Id = user.Id,
                Firstname = user.Firstname,
                Lastname = user.Lastname,
                Email = user.Email,
                Adress = user.Adress,
                Phone = user.Phone,
                Password = user.Password,
            };
            return View(model);
        }


        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public IActionResult Update(UserInfo newUser)
        {
            string userId = User.FindFirst(x => x.Type == ClaimTypes.UserData).Value;
            var Id = Convert.ToInt32(userId);

            var user = _userService.Get(Id);

            user.Firstname = newUser.Firstname;
            user.Lastname = newUser.Lastname;
            user.Phone = newUser.Phone;
            user.Adress = newUser.Adress;

            _userService.Update(user);

            return Redirect("/User/GetAccount");
        }

        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public IActionResult Delete(int number)
        {
            bool result = false;

            var user = _userService.Get(number);
            if (user != null)
            {
                _userService.Delete(user);
                result = true;
            }

            return Json(result);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult Recovery(UserInfo recUser)
        {
            bool result = false;

            var user = _userService.Get(recUser.Id);

            if (user != null)
            {
                user.IsDeleted = recUser.IsDelete;
                _userService.Update(user);
                result = true;
            }
            return Json(result);
        }
    }
}

﻿using CourseworkDB.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using WebApp.Models;
using WebApp.Models.Room;

namespace WebApp.Controllers
{
    public class RoomController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRoomService _roomService;
        private readonly IPictureService _pictureService;

        public RoomController(IUserService userService, IRoomService roomService, IPictureService pictureService)

        {
            _userService = userService;
            _roomService = roomService;
            _pictureService = pictureService;
        }

        [HttpGet]
        public IActionResult GetInfo(int id)
        {
            var room = _roomService.Get(id);
            var model = new RoomInfo()
            {
                Id = room.Id,
                Name = room.Name,
                Adress = room.Adress,
                ActualPrice = room.ActualPrice,
                Desc = room.Desc,
                Pictures = _pictureService.GetPicRoom(id),
            };

            return View(model);
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var room = _roomService.GetAll();
            var model = new List<RoomInfo>();

            model = room
                .Select(u => new RoomInfo
                {
                    Id = u.Id,
                    Adress = u.Adress,
                    Name = u.Name,
                    ActualPrice = u.ActualPrice,
                    Desc = u.Desc,
                    Owner = u.Owner.Lastname,
                    Pictures = _pictureService.GetPicRoom(u.Id),
                })
            .ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public IActionResult Create(RoomInfo updRoom)
        {

            var id = Convert
                .ToInt32
                (User
                .FindFirst(x => x.Type == ClaimTypes.UserData)
                .Value);

            var room = new Room()
            {
                Serial = _roomService.RandomSerial(),
                Name = updRoom.Name,
                Adress = updRoom.Adress,
                ActualPrice = updRoom.ActualPrice,
                Desc = updRoom.Desc,

                Owner = _userService.Get(id),
            };

            _roomService.Add(room);
            return RedirectToAction("GetUserAll");
        }

        [Authorize(Roles = "User, Admin")]
        [HttpPost]
        public IActionResult Update(RoomInfo room)
        {
            var result = false;

            var updRoom = _roomService.Get(room.Id);

            updRoom.Name = room.Name;
            updRoom.Adress = room.Adress;
            updRoom.ActualPrice = room.ActualPrice;
            updRoom.Desc = room.Desc;

            if (updRoom != null)
            {
                result = true;
                _roomService.Update(updRoom);

            }

            return Json(result);
        }

        public IActionResult GetUserAll()
        {
            var id = Convert
                .ToInt32
                (User
                .FindFirst(x => x.Type == ClaimTypes.UserData)
                .Value);

            var room = _roomService.GetAll().Where(i => i.Owner.Id == id).ToList();
            var model = new List<RoomInfo>();

            model = room
                .Select(u => new RoomInfo
                {
                    Id = u.Id,
                    Adress = u.Adress,
                    Name = u.Name,
                    ActualPrice = u.ActualPrice,
                    Desc = u.Desc,
                    Owner = u.Owner.Lastname,
                    Pictures = _pictureService.GetPicRoom(u.Id).ToList(),
                })
            .ToList();

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "User, Admin")]
        public IActionResult Delete(int id)
        {
            bool result = false;

            var room = _roomService.Get(id);
            if (room != null)
            {
                _roomService.Delete(room);
                result = true;
            }

            return Json(result);
        }


        [HttpGet]
        public IActionResult GetFree(DateTime st, DateTime et)
        {
            var room = _roomService.GetFree(st, et);
            var model = new List<RoomInfo>();
            model = room
                .Select(u => new RoomInfo
                {
                    Id = u.Id,
                    Adress = u.Adress,
                    Name = u.Name,
                    ActualPrice = u.ActualPrice,
                    Owner = u.Owner.Lastname,
                    Pictures = _pictureService.GetPicRoom(u.Id).ToList(),
                })
            .ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult SaveImg()
        {
            return View();
        }


        [HttpPost]
        public IActionResult SaveImg(BufferImage img)
        {
            //var result = false;

            var pic = new Picture { Name = img.Name };

            byte[] imageData = null;
            //object in byte
            using (var binaryReader = new BinaryReader(img.picFile.OpenReadStream()))
            {
                imageData = binaryReader.ReadBytes((int)img.picFile.Length);
            }

            pic.File = imageData;
            pic.Room = _roomService.Get(img.RoomId);
           
            //if (pic.File != null)
            //{
            //    result = true;
            //}

            _pictureService.Save(pic);

            var id = Convert
                            .ToInt32
                            (User
                            .FindFirst(x => x.Type == ClaimTypes.UserData)
                            .Value);

            var room = _roomService.GetAll().Where(i => i.Owner.Id == id).ToList();
            var model = new List<RoomInfo>();

            model = room
                .Select(u => new RoomInfo
                {
                    Id = u.Id,
                    Adress = u.Adress,
                    Name = u.Name,
                    ActualPrice = u.ActualPrice,
                    Desc = u.Desc,
                    Owner = u.Owner.Lastname,
                    Pictures = _pictureService.GetPicRoom(u.Id).ToList(),
                })
            .ToList();

            //return Json(result);
            return View("GetUserAll", model);
        }

        [HttpGet]
        public IActionResult GetImage(int id)
        {

            var img = _pictureService
                .GetPicRoom(id).
                ToList();

            var model = new List<BufferImage>();

            model = img.Select(u => new BufferImage
            {
                Name = u.Name,
                Image = u.File,
                Id = u.Id,

            }).ToList();

            return View(model);
        }

    }
}
﻿using CourseworkDB;
using CourseworkDB.Model;
using Services.Interfaces;
using System;

namespace Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _uow;

        public RoleService(IUnitOfWork uow)
        {
            _uow = uow;
        }



        public Role Read(int id)
        {
            return _uow.Roles.Read(id);
        }
    }
}

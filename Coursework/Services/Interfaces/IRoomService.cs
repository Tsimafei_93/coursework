﻿using CourseworkDB.Model;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IRoomService
    {
        public List<RoomDto> GetAll();
        public Room Get(int id);
        public Room Add(Room room);
        public Room Update(Room room);
        public void Delete(Room room);
        public string RandomSerial();
        public List<RoomDto> GetFree(DateTime st, DateTime et);
    }
}
 
﻿using CourseworkDB.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IPictureService
    {
        public List<Picture> GetPic();
        public void Delete(Picture pic);
        public Picture Save(Picture pic);
        public List<Picture> GetPicRoom(int roomId);
    }
}

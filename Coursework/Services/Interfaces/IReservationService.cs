﻿using CourseworkDB.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IReservationService
    {
        public List<Reservation> ReadAllReservation();
        public Reservation Add(Reservation res);
        public Reservation Read(int id);
        public List<ReservationDto> GetAll();
    }
}

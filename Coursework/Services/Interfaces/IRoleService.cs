﻿using CourseworkDB.Model;

namespace Services.Interfaces
{
    public interface IRoleService
    {
        public Role Read(int id);
    }
}

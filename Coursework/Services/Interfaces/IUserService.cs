﻿using CourseworkDB.Model;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IUserService
    {
        //public List<User> GetAll();
        public List<UserDto> GetAll();
        public User Get(int id);
        public User Add(User user);
        public User Update(User user);
        public void Delete(User user);
    }
}

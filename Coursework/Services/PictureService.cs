﻿using CourseworkDB;
using CourseworkDB.Model;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Services
{
    public class PictureService : IPictureService
    {
        private readonly IUnitOfWork _uow;

        public PictureService(IUnitOfWork uow)
        { 
            _uow = uow;
        }

        public Picture Save(Picture pic)
        {
            _uow.Pictures.Create(pic);
            _uow.SaveChanges();
            return pic;
        }

        public void Delete(Picture pic)
        {
            _uow.Pictures.Delete(pic);
        }

        public List<Picture> GetPic()
        {
            return _uow.Pictures
                .ReadAll().ToList();
        }

        public List<Picture> GetPicRoom(int roomId)
        {
            var room = _uow.Rooms.Read(roomId);
            return _uow.Pictures
                .ReadAll().Where(u=>u.Room==room).ToList();
        }
    }
}

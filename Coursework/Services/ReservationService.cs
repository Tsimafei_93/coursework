﻿using CourseworkDB;
using CourseworkDB.Model;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{

    public class ReservationDto
    {
        public int Id { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public UsersDto Client { get; set; }

        public RoomsDto Room { get; set; }
    }

    public class UsersDto
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }

    public class RoomsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
    }




    public class ReservationService : IReservationService
    {
        private readonly IUnitOfWork _uow;

        public ReservationService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Reservation Add(Reservation res)
        {
            _uow.Reservations.Create(res);
            _uow.SaveChanges();
            return res;
        }

        public List<Reservation> ReadAllReservation()
        {
            return _uow.Reservations
                .ReadAll()
                .ToList();
        }

        public List<ReservationDto> GetAll()
        {
            var result = new List<ReservationDto>();
            var reser = _uow.Reservations
                .ReadAll();

            foreach (var res in reser.Include(u => u.Room).Include(u => u.Client).ToList())
            {
                var resDto = new ReservationDto
                {
                    Id = res.Id,
                    StartTime = res.StartTime,
                    EndTime = res.EndTime,
                    Client = new UsersDto
                    {
                        Lastname = res.Client.Lastname,
                        Firstname = res.Client.Firstname,
                        Id = res.Client.Id,
                    },
                    Room = new RoomsDto
                    {
                        Id = res.Room.Id,
                        Name = res.Room.Name,
                        Adress = res.Room.Adress,

                    }
                };
                result.Add(resDto);
            }

            return result;
        }

        public Reservation Read(int id)
        {
            return _uow.Reservations
               .Read(id);
        }
    }
}

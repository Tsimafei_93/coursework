﻿using CourseworkDB;
using CourseworkDB.Model;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class RoomDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Serial { get; set; }
        public string Adress { get; set; }
        public decimal ActualPrice { get; set; }
        public string Desc { get; set; }

        public OwnerDto Owner { get; set; }

    }

    public class OwnerDto
    {
        public string Lastname { get; set; }
        public int Id { get; set; }
    }



    public class RoomService : IRoomService
    {

        static readonly Random rndGen = new Random();
        public string RandomSerial()
        {
            const string ch = "qwertyuiopasdfghjklzxcvbnm0123456789";
            char[] pwd = new char[15];
            for (int i = 0; i < pwd.Length; i++)
                pwd[i] = ch[rndGen.Next(ch.Length)];
            return new string(pwd);
        }

        private readonly IUnitOfWork _uow;

        public RoomService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Room Add(Room room)
        {
            _uow.Rooms.Create(room);
            _uow.SaveChanges();
            return room;
        }

        public Room Get(int id)
        {
            return _uow.Rooms
                .Read(id);
        }

        public List<RoomDto> GetAll()
        {
            var result = new List<RoomDto>();
            var rooms = _uow.Rooms
                .readAll();

            foreach (var room in rooms.Include(u => u.Owner).ToList())
            {
                var roomDto = new RoomDto
                {
                    Id = room.Id,
                    Name = room.Name,
                    Serial = room.Serial,
                    Adress = room.Adress,
                    ActualPrice = room.ActualPrice,
                    Desc = room.Desc,
                    Owner = new OwnerDto
                    {
                        Lastname = room.Owner?.Lastname,
                        Id = room.Owner.Id,
                    }
                };
                result.Add(roomDto);
            }

            return result;
        }



        public Room Update(Room room)
        {
            _uow.Rooms.Update(room);
            _uow.SaveChanges();
            return room;
        }

        public void Delete(Room room)
        {
            _uow.Rooms.Delete(room);
            _uow.SaveChanges();
        }


        public List<RoomDto> GetFree(DateTime st, DateTime et)
        {

            var reservations = new List<int>();
            while (st <= et)
            {
                var res = _uow.Reservations.ReadAll().Where(u => u.StartTime != st && u.EndTime != et);

                foreach (var rese in res.Include(u => u.Room).ToList())
                {
                    var resDto = new ReservationDto
                    {
                        Room = new RoomsDto
                        {
                            Id = rese.Room.Id,
                        }
                    };
                    reservations.Add(resDto.Room.Id);
                }
                st = st.AddDays(1);
            };

            var resDis = reservations.Distinct();

            var freeRooms = new List<RoomDto>();

            var rooms = _uow.Rooms
                .readAll();

            foreach (var room in rooms.Include(u => u.Owner).ToList())
            {
                var busy = false;

                var roomDto = new RoomDto
                {
                    Id = room.Id,
                    Name = room.Name,
                    Serial = room.Serial,
                    Adress = room.Adress,
                    Owner = new OwnerDto
                    {
                        Lastname = room.Owner?.Lastname,
                        Id = room.Owner.Id,
                    }
                };

                for (int i = 0; i < resDis.Count(); i++)
                {
                    var a = resDis.ElementAt(i);
                    if (roomDto.Id == a)
                    {
                        busy = true;
                        break;
                    }
                }

                if (busy == false)
                    freeRooms.Add(roomDto);
            }

            return freeRooms;
        }
    }
}

﻿using CourseworkDB;
using CourseworkDB.Model;
using Microsoft.EntityFrameworkCore;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public bool IsDelete { get; set; }
        public RoleDto Role { get; set; }
        public ICollection<Room> Rooms { get; set; }
    }

    public class RoleDto
    {
        public string Name { get; set; }
    }

    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;

        public UserService(IUnitOfWork uow)
        {
            _uow = uow;
        }


        //Create user
        public User Add(User user)
        {
            _uow.Users.Create(user);
            _uow.SaveChanges();
            return user;
        }

        //Find user
        public User Get(int id)
        {
            return _uow.Users
                  .Read(id);
        }

        //Get all users
        public List<UserDto> GetAll()
        {
            var result = new List<UserDto>();
            var users = _uow.Users
                .ReadAll();
            foreach (var user in users.Include(u => u.Role).ToList())
            {
                var userDto = new UserDto
                {
                    Id = user.Id,
                    Firstname = user.Firstname,
                    Lastname = user.Lastname,
                    Password = user.Password,
                    Email = user.Email,
                    Adress = user.Adress,
                    Phone = user.Phone,
                    IsDelete = user.IsDeleted,
                    Role = new RoleDto
                    {
                        Name = user.Role?.Name,
                    }
                };
                result.Add(userDto);
            }

            return result;
        }

        //Update user
        public User Update(User user)
        {
            _uow.Users.Update(user);
            _uow.SaveChanges();
            return user;
        }

        public void Delete(User user)
        {
            _uow.Users.Delete(user);
            _uow.SaveChanges();
        }
    }
}
